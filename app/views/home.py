# -*- coding: utf-8 -*-
"""
Created on Sun Oct  8 21:56:53 2017

@author: slava
"""

from flask import render_template
from app import app, socketio

@app.route('/')
def home():
    return render_template('home.html')

@socketio.on('connect')
def connect():
    print('Use somethink')

usuarios = []
points = []
    
@socketio.on('addUsuario')
def addUsuario(usuario):
    
    usuarios.append(usuario)
    
    socketio.emit('usuarios', usuarios, broadcast = True)

@socketio.on('updateUser')
def updateuser(u):
    
    for i, usr in enumerate(usuarios, start = 0):
        
        if usr['id'] == u['id']:
#            print(u, "DDDDD")
            usuarios[i] = u
            break
    
    socketio.emit('usuarios', usuarios, broadcast = True)

@socketio.on('drawPoint')
def drawPoint(point):    
    points.append(point)

    socketio.emit('points', points, broadcast = True)