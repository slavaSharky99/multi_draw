# -*- coding: utf-8 -*-
"""
Created on Sun Oct  8 21:46:30 2017

@author: slava
"""

from flask import Flask
from flask_socketio import SocketIO

app = Flask(__name__, template_folder = 'template', static_folder = 'static')
socketio = SocketIO(app)

from app.views import home