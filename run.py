# -*- coding: utf-8 -*-
"""
Created on Sun Oct  8 21:50:05 2017

@author: slava
"""

from app import app, socketio

if __name__ == "__main__":
    socketio.run(app)